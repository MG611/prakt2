import math
import os

def main():
    print("Меню")
    print("Введите команду: 1 - Калькулятор, 2 - Подсчёт в строке, 3 - Матрица, 0 - Выход")
    cmd1 = input("Введите команду: ")
    cmd = int(cmd1)
    if cmd >= 0 and cmd < 4:
        while cmd != 0:
            if cmd == 1:
                calc()
            elif cmd == 2:
                troka()
            elif cmd == 3:
                printMatrix()
            else:
                print("Такой команды нет!")
        exit()
    else:
        print("Введите правильную команду!")
        main()
    return

def calc():
    print("Калькулятор")
    print("Введите команду: 1 - Стандартные арифметические операции, 2 - Четыре метода из math, 3 - Площадь и периметр параллелепипеда")
    calc2 = input("Введите команду калькулятора: ")
    calc1 = int(calc2)
    if calc1 >= 0 and calc1 < 4:
        while calc1 != 0:
            if calc1 == 1:
                Firstcalc()
            elif calc1 == 2:
                Secondcalc()
            elif calc1 == 3:
                Thirdcalc()
            main()
    else:
        print("Введите правильную команду!")
        clear()
        main()
    return

def Firstcalc():
    print("Стандартные арифметические операции")
    y = input("Введите первое число: ")
    y1 = input("Введите первое число: ")
    x = int(y)
    x1 = int(y1)
    print("Введите знак: -, +, /, *, // - Деление нацело, % - Вывод остатка операции")
    unit = input("Введите знак: ")
    def get_multiplier(unit):
        if unit == "-":
            return x-x1
        if unit == "+":
            return x+x1
        if unit == "/":
            return x/x1
        if unit == "*":
            return x*x1
        if unit == "//":
            return x//x1
        if unit == "%":
            return x%x1
        if unit == "**":
            return x**x1
    print(get_multiplier(unit))

def Secondcalc():
    print("Четыре метода из math")
    y = input("Введите первое число: ")
    y1 = input("Введите второе число: ")
    x = int(y)
    x1 = int(y1)
    print("Введите команду: 1 - Возведение в степень, 2 - Вычисление логарифма, 3 - Вывод факториала числа x, 4 - Округление до большего целого числа x")
    unit = input("Введите команду: ")
    def get_multiplier(unit):
        if unit == "1":
            return math.pow(x, x1)
        if unit == "2":
            return math.log(x, x1)
        if unit == "3":
            return math.factorial(x)
        if unit == "4":
            return math.ceil(x)
    print(get_multiplier(unit))

def Thirdcalc():
    print("Площадь и периметр параллелепипеда")
    print("Введите исходные данные: ")
    a = float(input('Длина (см) '))
    b = float(input('Ширина (см) '))
    h = float(input('Высота (см) '))
    print('Площадь параллелепипеда', 2 * (a * b + a * h + b * h) , 'кв.см')
    print('Периметр параллелепипеда', 4 * (a + b + h) , 'кв.см')

def troka():
    stroka = input("Введите текст: ")
    print("Количество символов: ", len(stroka))
    print("Количество пробелов: ", stroka.count(" "))
    print("Количество запятых: ", stroka.count((",")))
    main()

def printMatrix ():

    stolb = int(input('Введите количество столбцов: '))
    strok = int(input('Введите количество строк: '))
    nach = int(input('Введите число начала матрицы: '))
    hod = int(input('Введите шаг увеличения: '))

    for i in range(strok):
        print("")
        for j in range(stolb):
            print(nach," ", end = " ")
            nach+=hod
    print(" ")
    main()


main()